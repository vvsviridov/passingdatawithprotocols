//
//  ViewController.swift
//  fieldAtoFiledB
//
//  Created by Владимир Свиридов on 13.12.2021.
//

import UIKit

class ViewControllerA: UIViewController {
    
    var delegate: DelegateForAandB?
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var button: UIButton!
    
    func setupButton() {
        button.layer.cornerRadius = 10;
        button.clipsToBounds = true
    }
    
    func setupTopItem() { // Убрать название кнопки назад "<Back", чтобы было просто "<"
        if let topItem = navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            topItem.backBarButtonItem?.tintColor = .red
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton()
        setupTopItem()
        // Do any additional setup after loading the view.
    }
    
    func changeLabel() {
        delegate?.setText(text: textField.text)
    }
    
    @IBAction func actionButton(_ sender: UIButton) {
        changeLabel()
        dismiss(animated: true)
    }
}

