//
//  DelegateForAandB.swift
//  fieldAtoFiledB
//
//  Created by Владимир Свиридов on 13.12.2021.
//

import UIKit

protocol DelegateForAandB {
    func setText(text: String?)
}
