//
//  ViewControllerB.swift
//  fieldAtoFiledB
//
//  Created by Владимир Свиридов on 13.12.2021.
//

import UIKit

class ViewControllerB: UIViewController {

    @IBOutlet weak var labelText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
     // MARK: - Navigation
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if let view = segue.destination as? ViewControllerA {
             view.delegate = self
         }
     }
    
}

extension ViewControllerB: DelegateForAandB {
    func setText(text: String?) {
        labelText.text = text
    }
}
